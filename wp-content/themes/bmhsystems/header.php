<? //ob_start(['FW_Hooks', '_minify_html']); ?>
<!doctype html>
<? if( KANTALOUP_BRANDING ) { ?><!-- Développé avec ❤ par kantaloup | Developed with ❤ by kantaloup --><? } ?>
<html lang="<?= LANG; ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<? /* Chrome navigation bar in Android
	<meta name="theme-color" content="#b3268a" />
 	*/ ?>

	<title><? is_front_page() ? the_title() : wp_title(''); ?> | <? bloginfo('name'); ?></title>
	<link rel="stylesheet" href="https://use.typekit.net/sdt6hep.css">

	<? include THEME_PATH . '/includes/favicon.php'; ?>

	<? wp_head(); ?>
</head>

<?
$body_classes = ! is_front_page() ? 'inner-page' : '';
?>
<body <? body_class($body_classes); ?>>
<div id="megamenu-pane"></div>

<script>
	window.ajax_url = '<?= admin_url('admin-ajax.php'); ?>';
	window.infinite_finished_message = '<?= pll__('infinite-scroll-finished'); ?>';
	window.google_marker_img = '<?= THEME_URL; ?>/images/icon-google-marker.svg';
	window.partners_slider_speed = <?= get_field('partners-slider-speed', 'option') ? get_field('partners-slider-speed', 'option') : 10000; ?>;
</script>

<header id="header">
	<div class="v-align d-flex align-items-center justify-content-between">
		<a id="site-logo" href="<?= pll_home_url(); ?>">
			<?= FW::get_image( get_field('site-logo-' . LANG, 'option'), [], '', 'white-logo' ); ?>
			<?= FW::get_image( get_field('inner-page-logo-' . LANG, 'option'), [], '', 'blue-logo' ); ?>
		</a>

		<nav id="site-nav">
			<ul>
				<?
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'container' => '',
						'items_wrap' => '%3$s'
					)
				);
				?>
			</ul>
		</nav>

		<a href="javascript:;" class="mobile-menu-trigger">
			<?= file_get_contents( THEME_PATH . '/images/icon-mobile-menu.svg' ); ?>
		</a>
	</div>

	<? if( get_field('phone', 'option') ){ ?>
		<a href="tel:<?= str_replace(['-', '.', ' ', '(', ')'], '', get_field('phone', 'option') ); ?>" id="header-tel"><?= get_field('phone', 'option'); ?></a>
	<? } ?>

</header>
<?php if(get_field('covid_'.LANG, 'option')): ?>
    <section class="covid">
        <div class="wrapper">
            <?= get_field('covid_'.LANG, 'option') ?>
            <img src="<?= THEME_URL . '/images/x-close.svg'; ?>" class="close-modal">
        </div>
    </section>
<?php endif; ?>
<nav id="mobile-menu">
	<a href="javascript:;" class="mobile-menu-trigger">&times;</a>

	<ul>
		<?
		wp_nav_menu(
			array(
				'theme_location' => 'mobile',
				'container' => '',
				'items_wrap' => '%3$s'
			)
		);
		?>
	</ul>
</nav>

<? include(THEME_PATH . '/includes/megamenu.php'); ?>

<main id="main">

