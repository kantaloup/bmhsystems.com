<? get_header(); ?>

	<div class="slice light-grey">
		<div class="the-content medium-grey">
			<?= get_field('404-content-' . LANG, 'option'); ?>
		</div>
	</div>

<? get_footer(); ?>