(function($){
	$(document).ready(function () {

		$('.upload-php .media-icon img').css({
			'width': 60,
			'height': 60
		});

		/*
		 * Hide the Admin Columns "Edit Columns" button on CPT archives for non admins
		 */
		if( ! $('body').hasClass('user-role-administrator') ) {
			$('a.cpac-edit:first-of-type').hide();
		}

		/*
		 * Hide the AAM box
		 */
		$('#advanced-sortables').hide();

		/*
		 * Hide the "Update" button on the Algolia options page
		 */
		$('body.options_page_acf-options-algolia #postbox-container-1').hide();

		/*
		 * Block the submit of the Algolia options page
		 */
		$('body.options_page_acf-options-algolia form#post').submit(function(e){
			e.preventDefault();
			e.stopPropagation();

			return false;
		});
	});
})(jQuery);