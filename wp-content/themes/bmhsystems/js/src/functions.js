/*
 * Trigger infinite scroll
 *
 * Usage:
 * $(window).scroll(function(){
		infinite_scroll($('.example'), function(){
			// load more stuff with ajax
		});
	});
 */
function infinite_scroll( el, _callback )
{
	if( ! el.length ) {
		return;
	}

	let bottom = el.offset().top + el.height();
	let scrolled = $(document).scrollTop() + $(window).height();

	// If you're within 300px of the bottom
	if( scrolled > (bottom - 300) ) {
		_callback();
	}
}

/*
 * Validates a form by checking that all of the required fields are filled
 */
function validate_form( form )
{
	form.removeClass('errors');
	form.find('.field').removeClass('error');

	let valid = true;

	form.find('.required').each(function(){
		let input = $(this);
		let parent = input.parents('.field');

		switch( input.attr('type') ) {
			case 'text':
			case 'tel':
			case 'email':
				if( input.val().trim() == '' ) {
					parent.addClass('error');
					valid = false;
				}
				break;

			case 'radio':
			case 'checkbox':
				let checkbox_name = input.attr('name');
				if( ! $('input[name="'+checkbox_name+'"]').is(':checked') ) {
					parent.addClass('error');
					valid = false;
				}
				break;

			case 'file':
				if( input.get(0).files.length === 0 ) {
					parent.addClass('error');
					valid = false;
				}
				break;
		}
	});

	return valid;
}

/*
 * Gathers all of the data from a form and sends it to the server via ajax.
 * Put your wp ajax action name in the data-action attr of the <form>.
 */
function handle_form( form, _callback )
{
	// Enter loading state
	form.addClass('loading');

	let form_data = new FormData();

	// Add form action
	form_data.append('action', form.attr('data-action'));

	// Add all fields
	form.serializeArray().reduce(function(obj, item) {
		form_data.append(item.name, item.value);
	}, {});

	// If the form contains files, add them to the form data
	if( form.find('input[type="file"]').length ){
		let file_fields = form.find('input[type="file"]');

		$.each(file_fields, function(index, element){
			if( $(element).get(0).files.length !== 0 ) {
				let field_name = $(element).attr('name');

				form_data.append(field_name, $(element)[0].files[0]);
			}
		});
	}

	// for debugging the form_data
	//new Response(form_data).text().then(console.log);

	// ajax
	$.ajax({
		url: window.ajax_url,
		data: form_data,
		type: "POST",
		processData: false,
		contentType: false,
		success: function (r, textStatus, jqXHR) {
			r = $.parseJSON(r);

			_callback(r);
		}
	});
}

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable	: false,
		zoom		: 16,
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		styles		: [ { "featureType": "administrative", "elementType": "all", "stylers": [ { "saturation": "-100" } ] }, { "featureType": "administrative.province", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }, { "color": "#f2f4f8" } ] }, { "featureType": "poi", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": "50" }, { "visibility": "simplified" }, { "color": "#d0d4db" } ] }, { "featureType": "road", "elementType": "all", "stylers": [ { "saturation": "-100" } ] }, { "featureType": "road.highway", "elementType": "all", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.arterial", "elementType": "all", "stylers": [ { "lightness": "30" } ] }, { "featureType": "road.local", "elementType": "all", "stylers": [ { "lightness": "40" } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "saturation": -100 }, { "visibility": "simplified" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "lightness": -25 }, { "saturation": -97 }, { "color": "#dfe2e7" } ] }, { "featureType": "water", "elementType": "labels", "stylers": [ { "lightness": -25 }, { "saturation": -100 } ] } ]
	};


	// create map
	var map = new google.maps.Map( $el[0], args);


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

		add_marker( $(this), map );

	});


	// center map
	center_map( map );


	// return
	return map;

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	var custom_marker = {
		url: window.google_marker_img,
		scaledSize: new google.maps.Size(46, 69), // scaled size
		origin: new google.maps.Point(0,0), // origin
		anchor: new google.maps.Point(0, 0) // anchor
	}

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon		: custom_marker
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
		map.setCenter( bounds.getCenter() );
		map.setZoom( 15 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

function header_class()
{
	if( $(window).scrollTop() > 0 ){
		$('#header, #megamenu').addClass('active');
		$('#sidebar').addClass('scrolled');
	} else {
		$('#header, #megamenu').removeClass('active');
		$('#sidebar').removeClass('scrolled');
	}
}

function init_dropdowns()
{
	$(".gfield_select").chosen({disable_search_threshold: 10});
}

function toggles()
{
	$('.toggles-nav a').click(function(e){
		e.preventDefault();

		var targetted_toggle = $(this).attr('href');
		targetted_toggle = targetted_toggle.replace('#', '');

		$('.toggles-nav a').removeClass('active');
		$(this).addClass('active');


		$('#custom-toggles .toggle').removeClass('active');
		$('#' + targetted_toggle).addClass('active');
	});
}

function megamenu()
{
	$('.show-megamenu').click(function(){

		if( $(window).width() < 1024 ) return;

		var this_link = $(this).find('> a');
		this_link.attr('href', 'javascript:;');

		$('#megamenu, #megamenu-pane').toggleClass('show');
	});
}

function mobile_menu()
{
	$('.mobile-menu-trigger').click(function(){
		$('#mobile-menu').toggleClass('active');
	});
}

function google_maps()
{
	$('.google-map').each(function(){
		map = new_map( $(this) );
	});
}

function bg_videos()
{
	$('.bg-video').each(function(){
		var yt_vid_id = $(this).data('video-id');

		$(this).YTPlayer({
			fitToBackground: true,
			videoId: yt_vid_id
		});

	});
}

function sticky_scroller(element)
{
	var window_top = $(window).scrollTop();
	var top = element.prev().offset().top;
	if (window_top > top) {
		element.addClass('stick');
	} else {
		element.removeClass('stick');
	}
}