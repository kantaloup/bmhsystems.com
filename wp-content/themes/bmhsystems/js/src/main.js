﻿var loading_infinite = false;
var map = null;

$(document).ready(function(){
	/*
	 * WOW JS
	 */
	new WOW().init();
$('.covid .close-modal').click(function(e) {
$('.covid').css({'height': '0', 'padding' : '0'});
});
	/*
	 * Header class
	 */
	header_class();

	/*
	 * Chosen JS dropdowns
	 */
	init_dropdowns();

	/*
	 * Google Maps
	 */
	google_maps();

	/*
	 * Custom plants toggles
	 */
	//toggles();

	$("a[href^=\\#]").click(function(e) {
		e.preventDefault();

		var dest = $(this).attr('href');
		history.pushState(null, null, dest);

		$('html,body').animate({
			scrollTop: $(dest).offset().top - 90
		},'slow'); });

	/*
	 * Megamenu
	 */
	megamenu();
	$('#megamenu-pane').click(function(){
		$('#megamenu-pane, #megamenu').toggleClass('show');
	});

	/*
	 * Mobile menu
	 */
	mobile_menu();

	/*
	 * Mobile sidebars
	 */
	$('.sidebar-trigger').click(function(){
		$('#sidebar').toggleClass('active');
	});

	/*
	 * Background YouTube videos
	 */
	bg_videos();

	/*$('form.auto').submit(function(e){
		e.preventDefault();

		let form = $(this);

		// honeypot, switch it as needed
		let hp = form.find('input[name="age"]');
		if( hp.val() != '' ) {
			return false;
		}

		// validate the fields
		if( ! validate_form(form) ) {
			e.preventDefault();
			return false;
		}

		handle_form(form, function(response){
			form.addClass('success');
			form.find('.form-row').remove();
		});

		return false;
	});*/

	/*
	 * Sticky elements
	 */
	if( $('.toggles-nav').length ) {
		sticky_scroller($('.toggles-nav'));
	}

	/*
	 * Partner slider
	 */
	$('.partners-slider').marquee({
		duration: window.partners_slider_speed,
		gap: 20,
		delayBeforeStart: 0,
		direction: 'left',
		duplicated: true,
		pauseOnHover: true
	});
});

$(document).bind('gform_post_render', function(){
	init_dropdowns();
});

$(window).on('load', function(){

});

$(window).resize(function(){

});

$(window).scroll(function(){
	/*
	 * Megamenu
	 */
	if( $('#megamenu').is(':visible') ){
		$('#megamenu-pane, #megamenu').toggleClass('show');
	}


	/*
	 * Header class
	 */
	header_class();

	/*
	 * Infinite scroll
	 */
	// Infinite scroll
	let infinite_scroll_element = $('.infinite-scroll');

	infinite_scroll( infinite_scroll_element, function(){

		if( loading_infinite || infinite_scroll_element.hasClass('infinite-finished') ) {
			return;
		}

		loading_infinite = true;

		infinite_scroll_element.addClass('loading');

		let inject_into = infinite_scroll_element.data('inject-into');

		let action = infinite_scroll_element.data('action');
		let element = infinite_scroll_element.data('element');
		let tax = infinite_scroll_element.data('tax');
		let terms = infinite_scroll_element.data('terms');

		let used_ids = [];
		if( infinite_scroll_element.find(element).length ) {
			infinite_scroll_element.find(element).each(function(){
				used_ids.push( $(this).data('id') );
			});
		}

		let data = {
			'action': action,
			'tax': tax,
			'terms': terms,
			'used_ids': used_ids
		};

		$.post(window.ajax_url, data, function(r){
			loading_infinite = false;
			infinite_scroll_element.removeClass('loading');

			if(r.success){

				if( 'data' in r ) {
					infinite_scroll_element.find(inject_into).append(r.data);
				} else {
					infinite_scroll_element.addClass('infinite-finished');
					infinite_scroll_element.append('<p class="infinite-finished-message">' + window.infinite_finished_message + '</p>' );
				}

			}
		});
	});

	/*
	 * Sticky elements
	 */
	if( $('.toggles-nav').length ) {
		sticky_scroller($('.toggles-nav'));
	}
});
