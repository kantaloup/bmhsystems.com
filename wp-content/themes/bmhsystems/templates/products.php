<?
/*
 * Template Name: Products
 */

$products_page_id = FW::get_page_id_by_template('templates/products');

$is_category = isset($queried_category) && ! empty($queried_category) ? true : false;

$page_title = $is_category ? $queried_category->name : get_the_title($products_page_id);

get_header(); ?>

	<section id="page" class="with-sidebar">
		<? include(THEME_PATH . '/includes/products/sidebar.php'); ?>

		<div id="page-inner">
			<h1 class="title small dark-grey"><?= $page_title; ?></h1>

			<?
			$args['post_type'] = 'product';
			$args['posts_per_page'] = 6;
			$args['orderby'] = 'menu_order';
			$args['order'] = 'ASC';

			if( $is_category ) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $queried_category->taxonomy,
						'terms' => $queried_category_id
					)
				);
			}

			$products = new WP_Query($args);

			if( $products->have_posts() ){ ?>

				<div id="products-posts" class="infinite-scroll" data-action="ajax_load_more_products" data-inject-into=".row" data-element=".product-archive-block" <? if( $is_category ){ ?>data-tax="<?= $queried_category->taxonomy; ?>"<? } ?> <? if( $is_category ){ ?>data-terms="<?= $queried_category->term_id; ?>"<? } ?>>
					<div class="row">
						<?
						while( $products->have_posts() ){
							$products->the_post();

							include(THEME_PATH . '/includes/products/archive-block.php');
						}
						?>
					</div>
				</div>

			<? } wp_reset_postdata(); ?>
		</div>
	</section>

<? get_footer(); ?>