<?
/*
 * Template Name: ROI Calculator
 */
get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section class="image-banner" style="background-image: url(<?= get_field('banner-img'); ?>);">
		<div class="wrapper medium">
			<? if( get_field('banner-title') ){ ?>
				<h1 class="title small white wow fadeInUp"><?= get_field('banner-title'); ?></h1>
			<? } ?>

			<? if( get_field('banner-subtitle') ){ ?>
				<h2 class="title medium white wow fadeInUp" data-wow-delay="120ms"><?= get_field('banner-subtitle'); ?></h2>
			<? } ?>

			<? if( have_rows('banner-buttons') ){ ?>
				<div class="buttons wow fadeInUp" data-wow-delay="220ms">
					<?
					while( have_rows('banner-buttons') ){ the_row();
						echo FW::button( get_sub_field('btn'), 'btn blue' );
					}
					?>
				</div>
			<? } ?>
		</div>
	</section>

	<section class="slice">
		<div class="wrapper medium">
			<div class="row">
				<div class="col-12 col-lg-6">
					<h3 class="title small dark-grey wow fadeInUp"><?= get_field('title'); ?></h3>

					<div class="icon-content">
						<? if( get_field('icon') ){ ?>
							<div class="icon wow fadeInLeft">
								<?= FW::get_image( get_field('icon') ); ?>
							</div>
						<? } ?>

						<div class="content wow fadeInUp">
							<? if( get_field('content') ){ ?>
								<div class="the-content"><?= get_field('content'); ?></div>
							<? } ?>

							<? if( have_rows('buttons') ){ ?>
								<div class="buttons">
									<?
									while( have_rows('buttons') ){ the_row();
										echo FW::button( get_sub_field('btn'), 'btn blue' );
									}
									?>
								</div>
							<? } ?>
						</div>
					</div>
				</div>

				<div class="col-12 col-lg-6">
					<div class="landing-form wow fadeInUp" data-wow-delay="600ms">
						<?
						if( get_field('form-id') ) {
							gravity_form(get_field('form-id'), get_field('form-show-title'), get_field('form-show-description'), false, [], true);
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>

<? endwhile; endif; get_footer(); ?>