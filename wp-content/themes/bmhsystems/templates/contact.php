<?
/*
 * Template Name: Contact
 */
get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>
	<section class="two-by-two-content-blocks">
		<div class="row no-gutters">
			<div class="col-12 col-sm-6">
				<div class="content-block grey left">
					<div class="inner">
						<div class="wow fadeInUp">
							<?
							if( get_field('form-id') ) {
								gravity_form(get_field('form-id'), get_field('form-show-title'), get_field('form-show-description'), false, [], true);
							}
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-6">
				<div class="content-block right">
					<div class="inner">
						<? if( get_field('title') ){ ?>
							<h1 class="title small dark-grey wow fadeInUp" data-wow-delay="150ms"><?= get_field('title'); ?></h1>
						<? } ?>

						<? if( have_rows('contact-rows') ){ ?>
							<div class="contact-rows">

								<? while( have_rows('contact-rows') ){ the_row(); ?>
									<div class="contact-row">
										<? if( get_sub_field('subtitle') ){ ?>
											<h2 class="title smaller medium-grey wow fadeInUp" data-wow-delay="150ms"><?= get_sub_field('subtitle'); ?></h2>
										<? } ?>

										<? if( get_sub_field('content') ){ ?>
											<div class="the-content wow fadeInUp" data-wow-delay="300ms"><?= get_sub_field('content'); ?></div>
										<? } ?>
									</div>
								<? } ?>

							</div>
						<? } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?
	if( get_field('google-map') ){
		$google_map = get_field('google-map'); ?>

		<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAPS_API_KEY; ?>"></script>
		<section class="google-map">
			<div class="marker" data-lat="<?= $google_map['lat']; ?>" data-lng="<?= $google_map['lng']; ?>"></div>
		</section>

	<? } ?>

<? endwhile; endif; get_footer(); ?>
