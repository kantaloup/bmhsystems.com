<?
/*
 * Template Name: Custom Plants
 */
get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<div class="two-by-two-content-blocks">
		<div class="row no-gutters">

			<div class="col-12 col-sm-6">
				<div class="content-block light-grey" data-mh="content-block">

					<div class="inner">
						<? if( get_field('title') ){ ?>
							<h1 class="title small dark-grey wow fadeInUp"><?= get_field('title'); ?></h1>
						<? } ?>

						<? if( get_field('content') ){ ?>
							<div class="the-content wow fadeInUp"><?= get_field('content'); ?></div>
						<? } ?>

						<? if( have_rows('buttons') ){ ?>
							<div class="buttons wow fadeInUp" data-wow-delay="350ms">

								<?
								while( have_rows('buttons') ){
									the_row();

									echo FW::button( get_sub_field('btn'), 'btn blue' );
								}
								?>

							</div>
						<? } ?>
					</div>

				</div>
			</div>

			<div class="col-12 col-sm-6">
				<div class="content-block img" data-mh="content-block" style="background-image: url(<?= get_field('img'); ?>)"></div>
			</div>
		</div>
	</div>

	<section class="slice">
		<div class="wrapper">
			<? if( get_field('toggles-title') ){ ?>
				<h2 class="title small dark-grey wow fadeInUp"><?= get_field('toggles-title'); ?></h2>
			<? } ?>
		</div>

		<?
		$toggles = get_field('toggles');

		if( ! empty($toggles) ){ ?>

			<div class="toggles-nav">
				<div class="wrapper">
					<? $i = 0; foreach($toggles as $toggle){ ?>
						<a href="#<?= FW::slugify($toggle['btn-text']); ?>" class="btn grey <?= $i == 0 ? 'active' : ''; ?>" data-mh="toggles-nav"><?= $toggle['btn-text']; ?></a>
					<? $i++; } ?>
				</div>
			</div>

		<? } ?>
	</section>

	<? if( ! empty($toggles) ){ ?>
		<section id="custom-toggles">
			<div class="wrapper small">

				<?
				$i = 0;
				foreach($toggles as $toggle){ ?>

					<div id="<?= FW::slugify($toggle['btn-text']); ?>" class="toggle <?= $i == 0 ? 'active' : ''; ?>">

						<? if( ! empty($toggle['content-rows']) ){ ?>
							<div class="toggle-content-rows">
								<div class="toggle-spacer"></div>
								<h3 class="title small blue"><?= $toggle['btn-text']; ?></h3>

								<?
								$i = 0;
								foreach($toggle['content-rows'] as $content_row){ ?>

									<div class="toggle-content-row <?= $i % 2 == 0 ? 'odd' : 'even'; ?>">
										<div class="row align-items-center">
											<div class="col-12 col-sm-6 content">
												<? if( ! empty($content_row['title']) ){ ?>
													<div class="toggle-title"><?= $content_row['title']; ?></div>
												<? } ?>

												<? if( ! empty($content_row['subtitle']) ){ ?>
													<div class="toggle-subtitle"><?= $content_row['subtitle']; ?></div>
												<? } ?>

												<? if( ! empty($content_row['content']) ){ ?>
													<div class="the-content"><?= $content_row['content']; ?></div>
												<? } ?>

												<? if( ! empty($content_row['buttons']) ){ ?>
													<div class="buttons">

														<?
														foreach($content_row['buttons'] as $btn){
															$btn = $btn['btn'];

															echo FW::button( $btn, 'btn blue' );
														}
														?>

													</div>
												<? } ?>
											</div>

											<div class="col-12 col-sm-6 img">
												<?
												if( ! empty($content_row['img']) ){
													echo FW::get_image( $content_row['img'] );
												}
												?>
											</div>
										</div>
									</div>

								<? $i++; } ?>

							</div>
						<? } ?>

					</div>

				<? $i++; } ?>

			</div>
		</section>
	<? } ?>

<? endwhile; endif; get_footer(); ?>