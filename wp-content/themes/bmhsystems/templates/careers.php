<?
/*
 * Template Name: Careers
 */
get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?
	if( get_field('banner-video-file') ) {
		$video_url = get_field('banner-video-file');
	}
	?>
	<section id="careers-banner" style="background-image: url(<?= get_field('banner-video-img'); ?>)">
		<div class="overlay" <? if( get_field('overlay-opacity') ){ ?>style="opacity: <?= get_field('overlay-opacity') / 100; ?>"<? } ?>></div>

		<div class="wrapper">
			<div class="content inner">
				<? if( get_field('title') ){ ?>
					<h2 class="title small small-under dark-grey wow fadeInUp"><?= get_field('title'); ?></h2>
				<? } ?>

				<? if( get_field('subtitle') ){ ?>
					<h3 class="title smaller small-under medium-grey wow fadeInUp" data-wow-delay="100ms"><?= get_field('subtitle'); ?></h3>
				<? } ?>

				<? if( get_field('content') ){ ?>
					<div class="the-content wow fadeInUp" data-wow-delay="150ms"><?= get_field('content'); ?></div>
				<? } ?>

				<? if( have_rows('buttons') ){ ?>
					<div class="buttons wow fadeInUp" data-wow-delay="350ms">

						<?
						while( have_rows('buttons') ){
							the_row();

							$type = get_sub_field('btn-type');
							$data = $type == 'video' ? 'data-fancybox' : '';

							echo FW::button( get_sub_field('btn'), 'btn blue', $data );
						}
						?>

					</div>
				<? } ?>
			</div>
		</div>

		<video preload="auto" autoplay muted>
			<source src="<?= $video_url; ?>" type="video/mp4">
		</video>
	</section>

	<section id="page">
		<section id="career-benefits" class="slice">
			<? if( get_field('benefits-title') ){ ?>
				<h2 class="title small small-under dark-grey text-center wow fadeInUp"><?= get_field('benefits-title'); ?></h2>
			<? } ?>

			<div class="wrapper">
				<? include( THEME_PATH . '/includes/careers/benefits.php'); ?>
			</div>
		</section>

		<? include( THEME_PATH . '/includes/careers/bottom.php' ); ?>
	</section>

<? endwhile; endif; get_footer(); ?>