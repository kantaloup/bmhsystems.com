<?
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\Point\Center;
use Imagine\Image\ImageInterface;

/*
 * The class for manipulating images
 */
class FW_Images extends FW {

	private $output_dir 		= THEME_PATH . '/images/cache/';
	private $output_dir_http	= THEME_URL . '/images/cache/';
	private $output_filename	= '%FILENAME%-%OPERATION%-%VALUE%-%FILTERS%.%EXTENSION%';

	function __construct()
	{

	}

	/*
	 * See FW::image() for docs
	 */
	public function resize_image( $src, $value, $operation, $filters, $force )
	{
		$image_path = $this->get_image_path($src);

		// check cache to see if this operation=>value has been done before
		$cache_name = $this->generate_file_name($image_path, $value, $operation, $filters);

		// maybe return the cached file
		if( file_exists($this->output_dir . $cache_name) && !$force ) {
			return $this->output_dir_http . $cache_name;
		}

		// load resizer
		require_once THEME_PATH . '/fw/core/lib/php-image-resize/ImageResize.php';
		$image = new \Gumlet\ImageResize($image_path);

		// perform the operation
		switch( $operation ) {
			case 'resize':
				// has both width and height supplied
				if( ! empty($value[0]) && ! empty($value[1]) ) {
					$image->resizeToBestFit($value[0], $value[1]);
				}
				// has only width supplied
				else if( ! empty($value[0]) && empty($value[1]) ) {
					$image->resizeToWidth($value[0]);
				}
				// has only height supplied
				else if( empty($value[0]) && ! empty($value[1]) ) {
					$image->resizeToHeight($value[1]);
				}
				break;

			case 'crop':
				$image->crop($value[0], $value[1]);
				break;

			case 'scale':
				$image->scale($value);
				break;
		}

		// apply filters
		if( ! empty($filters) ) {
			foreach($filters as $filter => $filter_value) {

				switch( $filter ) {
					case 'grayscale':
						$image->addFilter(function($desc){
							imagefilter($desc, IMG_FILTER_GRAYSCALE);
						});
					break;
				}

			}
		}

		// save the new image
		$image->save($this->output_dir . $cache_name);

		// done
		return $this->output_dir_http . $cache_name;
	}

	/*
	 * Analyzes the $src and returns a local file path
	 *
	 * @param 	string|int	$src	Can be a URL, a local path, or an WordPress attachment ID
	 */
	public function get_image_path( $src )
	{
		// attachment_id
		if( is_int($src) && get_post_type($src) == 'attachment' ) {
			return get_attached_file($src);
		}
		// url
		else if( substr($src, 0, 4) == 'http' ) {
			$attch_id = FW::attachment_url_to_id($src);
			return get_attached_file($attch_id);
		}
		// local path
		else {
			return $src;
		}
	}

	/*
	 * Generates the file name for the cached image
	 */
	public function generate_file_name( $src, $value, $operation, $filters )
	{
		$pathinfo = pathinfo($src);

		if( is_array($value) ) {
			$operation_str = implode('_', $value);
		} else {
			$operation_str = $value;
		}

		$filename = $this->output_filename;
		$filename = str_replace('%FILENAME%', $pathinfo['filename'], $filename);
		$filename = str_replace('%OPERATION%', $operation, $filename);
		$filename = str_replace('%VALUE%', $operation_str, $filename);

		// if filters, add them to filename
		if( ! empty($filters) ) {
			$filters_str = '';

			foreach( $filters as $key => $filter_value ) {
				if( ! empty($key) ) {
					$filters_str .= "{$key}_{$filter_value}_";
				} else {
					$filters_str .= "{$filter_value}_";
				}
			}

			$filters_str = trim($filters_str, '_');
			$filename = str_replace('%FILTERS%', $filters_str, $filename);
		}
		// no filters, remove the placeholder
		else {
			$filename = str_replace('-%FILTERS%', '', $filename);
		}

		$filename = str_replace('%EXTENSION%', $pathinfo['extension'], $filename);

		return $filename;
	}
}