<?php
// Creating the widget
class kant_txt_with_btn extends WP_Widget {

	function __construct() {
		parent::__construct(
			'kant_txt_with_btn',
			'Texte avec bouton',
			array(
				'description' => 'Un bloc de texte avec un bouton'
			)
		);
	}

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$text = apply_filters( 'the_content', $instance['text'] );
		$btn = $instance['btn'];
		$url = $instance['url'];

		echo $args['before_widget'];

		if( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
			echo '<div class="the-content">' . $text . '</div>';
			echo '<a href="' . get_permalink($url) . '" class="simple-mint">' . $btn . '</a>';
		}

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
			$text = $instance[ 'text' ];
			$btn = $instance[ 'btn' ];
			$url = $instance[ 'url' ];
		}
		else {
			$title = 'Titre';
			$text = 'Contenu';
			$btn = 'Texte du bouton';
			$url = '#';
		}
		?>
		<p>
			<label for="<?= $this->get_field_id('title'); ?>"><?= 'Titre'; ?></label>
			<input class="widefat" id="<?= $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?= esc_attr($title); ?>">
		</p>
		<p>
			<label for="<?= $this->get_field_id('text'); ?>"><?= 'Contenu'; ?></label>
			<textarea rows="10" class="widefat" id="<?= $this->get_field_id('text'); ?>" name="<?= $this->get_field_name('text'); ?>" type="text"><?= esc_attr($text); ?></textarea>
		</p>
		<p>
			<label for="<?= $this->get_field_id('btn'); ?>"><?= 'Texte du bouton'; ?></label>
			<input class="widefat" id="<?= $this->get_field_id('btn'); ?>" name="<?= $this->get_field_name('btn'); ?>" type="text" value="<?= esc_attr($btn); ?>">
		</p>
		<p>
			<label for="<?= $this->get_field_id('url'); ?>"><?= 'Lien du bouton'; ?></label>
			<select id="<?= $this->get_field_id('url'); ?>" name="<?= $this->get_field_name('url'); ?>" class="widefat">
				<?
				$pages = get_pages(
					array(
						'posts_per_page' => -1
					)
				);
				foreach($pages as $page){
					if($url == $page->ID){
						$selected = true;
					} else {
						$selected = false;
					}
					if($selected) {
						echo '<option id="" value="' . $page->ID . '" selected>' . $page->post_title . '</option>';
					} else {
						echo '<option id="" value="' . $page->ID . '">' . $page->post_title . '</option>';
					}
				}
				?>
			</select>
		</p>

	<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		$instance['btn'] = ( ! empty( $new_instance['btn'] ) ) ? strip_tags( $new_instance['btn'] ) : '';
		$instance['url'] = ( ! empty( $new_instance['url'] ) ) ? strip_tags( $new_instance['url'] ) : '';
		return $instance;
	}
}

function kant_load_widget() {
	register_widget( 'kant_txt_with_btn' );
}
add_action('widgets_init', 'kant_load_widget');
