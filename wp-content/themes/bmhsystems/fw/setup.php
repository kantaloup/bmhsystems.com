<?
require_once 'core/framework.class.php';

/*
 * Init and override
 */
$fw = new FW(
	array(
		'queue_version' => 1.1,
		'queue' => ['jquery', 'wow', 'fancybox', 'match-height', 'chosen', 'sticky'],
		'options_pages' => ['General', 'Megamenu'],
		'more_js_files' => array(
			THEME_URL . '/js/src/lib/yt-video.js',
			THEME_URL . '/js/src/lib/jquery.marquee.min.js',
		)
	)
);

/*
 * Register strings
 */
$fw->register_strings([
	['kantaloup', 'read-more', 'General'],
	['kantaloup', 'infinite-scroll-finished', 'General'],
	['kantaloup', '© 2020 Astec Industries, Inc. – All Rights Reserved', 'General'],

	['kantaloup', 'latest-news', 'Home'],

	['kantaloup', 'back-to-blog', 'Blog'],
	['kantaloup', 'filter-by-category', 'Blog'],
	['kantaloup', 'other-article-from', 'Blog'],
	['kantaloup', 'share-this-article', 'Blog'],

	['kantaloup', 'back-to-projects', 'Projects'],
	['kantaloup', 'other-projcts-from', 'Projects'],

	['kantaloup', 'job-view-more', 'Careers'],

	['kantaloup', 'in-stock', 'Products'],
	['kantaloup', 'out-of-stock', 'Products'],
	['kantaloup', 'other-products-from', 'Products'],
	['kantaloup', 'filter-by-type', 'Products'],
	['kantaloup', 'back-to-products', 'Products'],
]);

/*
 * Register CPT's
 */
$fw->register_cpt('product', [
	'name'               => 'Products',
	'singular_name'      => 'Product',
	'add_new'            => 'Add new',
	'add_new_item'       => 'Add new',
	'edit_item'          => 'Edit',
	'new_item'           => 'New',
	'all_items'          => 'All',
	'view_item'          => 'View',
	'search_items'       => 'Search',
	'not_found'          => 'Not found',
	'not_found_in_trash' => 'Not found in trash',
	'parent_item_colon'  => '',
	'menu_name'          => 'Products'
],
[
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'product' ),
	'capability_type'    => 'post',
	'has_archive'        => false,
	'hierarchical'       => false,
	'menu_position'      => null,
	'supports'           => array( 'title', 'author', 'thumbnail', 'page-attributes' )
]);

$fw->register_cpt('project', [
	'name'               => 'Projects',
	'singular_name'      => 'Project',
	'add_new'            => 'Add new',
	'add_new_item'       => 'Add new',
	'edit_item'          => 'Edit',
	'new_item'           => 'New',
	'all_items'          => 'All',
	'view_item'          => 'View',
	'search_items'       => 'Search',
	'not_found'          => 'Not found',
	'not_found_in_trash' => 'Not found in trash',
	'parent_item_colon'  => '',
	'menu_name'          => 'Projects'
],
[
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'project' ),
	'capability_type'    => 'post',
	'has_archive'        => false,
	'hierarchical'       => false,
	'menu_position'      => null,
	'supports'           => array( 'title', 'author', 'thumbnail', 'page-attributes' )
]);

$fw->register_cpt('career', [
	'name'               => 'Careers',
	'singular_name'      => 'Career',
	'add_new'            => 'Add new',
	'add_new_item'       => 'Add new',
	'edit_item'          => 'Edit',
	'new_item'           => 'New',
	'all_items'          => 'All',
	'view_item'          => 'View',
	'search_items'       => 'Search',
	'not_found'          => 'Not found',
	'not_found_in_trash' => 'Not found in trash',
	'parent_item_colon'  => '',
	'menu_name'          => 'Careers'
],
[
	'public'             => true,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'rewrite'            => array( 'slug' => 'career' ),
	'capability_type'    => 'post',
	'has_archive'        => false,
	'hierarchical'       => false,
	'menu_position'      => null,
	'supports'           => array( 'title', 'author', 'thumbnail', 'editor' )
]);

/*
 * Register taxonomies
 */
$fw->register_taxonomies('product-category', 'product', [
	'label' => 'Catégories',
	'rewrite' => array( 'slug' => 'product-category' ),
	'hierarchical' => true
]);

$fw->register_taxonomies('product-type', 'product', [
	'label' => 'Types',
	'rewrite' => array( 'slug' => 'product-type' ),
	'hierarchical' => true
]);

$fw->register_taxonomies('project-category', 'project', [
	'label' => 'Catégories',
	'rewrite' => array( 'slug' => 'project-category' ),
	'hierarchical' => true
]);

/*
 * Register nav menus
 */
$fw->register_nav_menus([
	'primary' => 'Navigation primaire',
	'mobile' => 'Navigation mobile',
]);