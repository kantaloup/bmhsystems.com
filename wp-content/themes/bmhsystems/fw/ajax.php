<?
/*
 * Infinite scroll for blog posts
 */
add_action('wp_ajax_ajax_load_more_posts', 'ajax_load_more_posts');
add_action('wp_ajax_nopriv_ajax_load_more_posts', 'ajax_load_more_posts');
function ajax_load_more_posts()
{
	if( ! isset($_POST['used_ids']) ) {
		wp_send_json_error();
	}

	$args = array(
		'post_type' => 'post',
		'post__not_in' => $_POST['used_ids'],
		'posts_per_page' => 6
	);

	if( isset($_POST['tax']) && ! empty($_POST['tax']) && isset($_POST['terms']) && ! empty($_POST['terms']) ) {
		$args['tax_query'] = array(array(
			'taxonomy' => $_POST['tax'],
			'field' => 'term_id',
			'terms' => $_POST['terms']
		));
	}

	$blog = new WP_Query($args);

	// No more posts, return empty success
	if( ! $blog->have_posts() ) {
		wp_send_json_success();
	}

	ob_start();

	while( $blog->have_posts() ) {
		$blog->the_post();

		include(THEME_PATH . '/includes/blog/archive-block.php');
	}

	$markup = ob_get_clean();

	echo wp_send_json_success($markup);
}

/*
 * Infinite scroll for projects
 */
add_action('wp_ajax_ajax_load_more_projects', 'ajax_load_more_projects');
add_action('wp_ajax_nopriv_ajax_load_more_projects', 'ajax_load_more_projects');
function ajax_load_more_projects()
{
	if( ! isset($_POST['used_ids']) ) {
		wp_send_json_error();
	}

	$args = array(
		'post_type' => 'project',
		'post__not_in' => $_POST['used_ids'],
		'posts_per_page' => 6,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	if( isset($_POST['tax']) && ! empty($_POST['tax']) && isset($_POST['terms']) && ! empty($_POST['terms']) ) {
		$args['tax_query'] = array(array(
			'taxonomy' => $_POST['tax'],
			'field' => 'term_id',
			'terms' => $_POST['terms']
		));
	}

	$projects = new WP_Query($args);

	// No more posts, return empty success
	if( ! $projects->have_posts() ) {
		wp_send_json_success();
	}

	ob_start();

	while( $projects->have_posts() ) {
		$projects->the_post();

		include(THEME_PATH . '/includes/projects/archive-block.php');
	}

	$markup = ob_get_clean();

	echo wp_send_json_success($markup);
}

/*
 * Infinite scroll for products
 */
add_action('wp_ajax_ajax_load_more_products', 'ajax_load_more_products');
add_action('wp_ajax_nopriv_ajax_load_more_products', 'ajax_load_more_products');
function ajax_load_more_products()
{
	if( ! isset($_POST['used_ids']) ) {
		wp_send_json_error();
	}

	$args = array(
		'post_type' => 'product',
		'post__not_in' => $_POST['used_ids'],
		'posts_per_page' => 6,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	if( isset($_POST['tax']) && ! empty($_POST['tax']) && isset($_POST['terms']) && ! empty($_POST['terms']) ) {
		$args['tax_query'] = array(array(
			'taxonomy' => $_POST['tax'],
			'field' => 'term_id',
			'terms' => $_POST['terms']
		));
	}

	$projects = new WP_Query($args);

	// No more posts, return empty success
	if( ! $projects->have_posts() ) {
		wp_send_json_success();
	}

	ob_start();

	while( $projects->have_posts() ) {
		$projects->the_post();

		include(THEME_PATH . '/includes/products/archive-block.php');
	}

	$markup = ob_get_clean();

	echo wp_send_json_success($markup);
}