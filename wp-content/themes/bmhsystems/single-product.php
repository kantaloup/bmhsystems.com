<?
$current_type = get_the_terms(get_the_ID(), 'product-type');

get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section id="page">
		<div class="two-by-two-content-blocks">
			<div class="row no-gutters">

				<div class="col-12 col-sm-6">
					<div class="content-block img" data-mh="content-block" style="background-image: url(<?= wp_get_attachment_url( FW::featured_image() ); ?>)"></div>
				</div>

				<div class="col-12 col-sm-6">
					<div class="content-block light-grey right no-top" data-mh="content-block">

						<div class="inner">
							<?
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
							<h1 class="title small small-under dark-grey"><?= get_the_title(); ?></h1>

							<? if( ! empty($current_type) ){ ?>
								<h2 class="title small medium-grey"><?= $current_type[0]->name; ?></h2>
							<? } ?>

							<? if( ! get_field('hide-stock') ){ ?>
								<div class="stock-btn <?= get_field('in-stock') ? 'in-stock' :'out-of-stock'; ?>"><?= get_field('in-stock') ? pll__('in-stock') : pll__('out-of-stock'); ?></div>
							<? } ?>

							<? if( get_field('content') ){ ?>
								<div class="the-content"><?= get_field('content'); ?></div>
							<? } ?>

							<? if( have_rows('buttons') ){ ?>
								<div class="buttons wow fadeInUp" data-wow-delay="350ms">

									<?
									while( have_rows('buttons') ){
										the_row();

										$type = get_sub_field('btn-type');
										$data = $type == 'video' ? 'data-fancybox' : '';

										echo FW::button( get_sub_field('btn'), 'btn blue', $data );
									}
									?>

								</div>
							<? } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?
		if( have_rows('rows') ){
			while( have_rows('rows') ){
				the_row();

				include(THEME_PATH . '/includes/content/open.php');
				include(THEME_PATH . '/includes/content/' . get_row_layout() . '.php');
				include(THEME_PATH . '/includes/content/close.php');
			}
		}
		?>

		<?
		$current_category = get_the_terms( get_the_ID(), 'product-type');
		$current_category = $current_category[0];

		$projects = new WP_Query(
			array(
				'post_type' => 'product',
				'posts_per_page' => 3,
				'post__not_in' => [get_the_ID()],
				'tax_query' => array(
					array(
						'taxonomy' => $current_category->taxonomy,
						'terms' => $current_category->term_id
					)
				)
			)
		);

		if( $projects->have_posts() ){ ?>

			<section id="product-related" class="slice light-grey">
				<div class="wrapper medium">
					<h2 class="title medium dark-grey wow fadeInUp"><?= pll__('other-products-from'); ?> <span class="medium-grey"><?= $current_category->name; ?></h2></h2>

					<div class="blog-articles wow fadeInUp" data-wow-delay="100ms">
						<div class="row">
							<?
							while( $projects->have_posts() ){
								$projects->the_post();

								include(THEME_PATH . '/includes/products/archive-block.php');
							}
							?>
						</div>
					</div>
				</div>
			</section>

		<? } ?>

	</section>

<? endwhile; endif; get_footer(); ?>