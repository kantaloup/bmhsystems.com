<? get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<? /*
	<section class="slice">
		<div class="wrapper">
			<h1 class="title small dark-grey wow fadeInUp"><?= get_the_title(); ?></h1>
			<div class="the-content wow fadeInUp"><? the_content(); ?></div>
		</div>
	</section>
 	*/ ?>

	<section id="page">
		<?
		if( have_rows('rows') ){
			while( have_rows('rows') ){
				the_row();

				include(THEME_PATH . '/includes/content/open.php');
				include(THEME_PATH . '/includes/content/' . get_row_layout() . '.php');
				include(THEME_PATH . '/includes/content/close.php');
			}
		}
		?>
	</section>

<? endwhile; endif; get_footer(); ?>