<? get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section id="post-top">
		<div class="wrapper">
			<h1 class="title small dark-grey wow fadeInUp"><?= get_the_title(); ?></h1>

			<nav class="single-post-categories wow fadeInUp" data-wow-delay="100ms"><? the_category(', '); ?></nav>

			<time class="wow fadeInUp" data-wow-delay="200ms"><?= date_i18n( get_option('date_format'), get_the_time('U') ); ?></time>
		</div>
	</section>

	<? if( FW::featured_image() ){ ?>
		<section id="post-fi">
			<div class="wrapper medium wow fadeInUp" data-wow-delay="300ms">
				<?= get_the_post_thumbnail(); ?>
			</div>
		</section>
	<? } ?>

	<section id="post-content" class="slice">
		<div class="wrapper">
			<div class="the-content wow fadeInUp"><? the_content(); ?></div>
			<? include(THEME_PATH . '/includes/facebook-share.php'); ?>
		</div>
	</section>

	<?
	$current_category = get_the_terms( get_the_ID(), 'category');
	$current_category = $current_category[0];

	$blog = new WP_Query(
		array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'post__not_in' => [get_the_ID()],
			'tax_query' => array(
				array(
					'taxonomy' => $current_category->taxonomy,
					'terms' => $current_category->term_id
				)
			)
		)
	);

	if( $blog->have_posts() ){ ?>

		<section id="post-related" class="slice light-grey">
			<div class="wrapper medium">
				<h2 class="title medium dark-grey wow fadeInUp"><?= pll__('other-article-from') . ' ' . $current_category->name; ?></h2>

				<div class="blog-articles wow fadeInUp" data-wow-delay="100ms">
					<div class="row">
						<?
						while( $blog->have_posts() ){
							$blog->the_post();

							include(THEME_PATH . '/includes/blog/archive-block.php');
						}
						?>
					</div>
				</div>
			</div>
		</section>

	<? } ?>

<? endwhile; endif; get_footer(); ?>