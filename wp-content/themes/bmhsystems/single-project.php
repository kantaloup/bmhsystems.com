<? get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section id="page">
		<?
		if( have_rows('rows') ){
			while( have_rows('rows') ){
				the_row();

				include(THEME_PATH . '/includes/content/open.php');
				include(THEME_PATH . '/includes/content/' . get_row_layout() . '.php');
				include(THEME_PATH . '/includes/content/close.php');
			}
		}
		?>
	</section>

	<?
	$current_category = get_the_terms( get_the_ID(), 'project-category');
	$current_category = $current_category[0];

	$projects = new WP_Query(
		array(
			'post_type' => 'project',
			'posts_per_page' => 3,
			'post__not_in' => [get_the_ID()],
			'tax_query' => array(
				array(
					'taxonomy' => $current_category->taxonomy,
					'terms' => $current_category->term_id
				)
			)
		)
	);

	if( $projects->have_posts() ){ ?>

		<section id="project-related" class="slice light-grey">
			<div class="wrapper medium">
				<h2 class="title medium dark-grey wow fadeInUp"><?= pll__('other-projcts-from'); ?> <span class="medium-grey"><?= $current_category->name; ?></h2></h2>

				<div class="blog-articles wow fadeInUp" data-wow-delay="100ms">
					<div class="row">
						<?
						while( $projects->have_posts() ){
							$projects->the_post();

							include(THEME_PATH . '/includes/projects/archive-block.php');
						}
						?>
					</div>
				</div>
			</div>
		</section>

	<? } ?>

<? endwhile; endif; get_footer(); ?>