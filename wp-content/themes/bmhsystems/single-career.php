<? get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<section id="page">
		<div class="two-by-two-content-blocks">
			<div class="row no-gutters">

				<div class="col-12 col-sm-6" data-mh="content-block">
					<div class="content-block left">

						<div class="inner">
							<h2 class="title small small-under dark-grey wow fadeInUp"><?= get_the_title( FW::get_page_id_by_template('templates/careers') ); ?></h2>
							<h1 class="title smaller medium-grey wow fadeInUp" data-wow-delay="120ms"><?= get_the_title(); ?></h1>
							<div class="the-content wow fadeInUp" data-wow-delay="220ms"><? the_content(); ?></div>
						</div>

					</div>
				</div>

				<div class="col-12 col-sm-6 career-grey" data-mh="content-block">
					<? if( FW::featured_image() ){ ?>
						<div class="career-img" style="background-image: url(<?= wp_get_attachment_url( FW::featured_image() ); ?>)"></div>
					<? } ?>

					<div class="content-block right">

						<div class="inner wow fadeInUp" data-wow-delay="300ms">
							<?
							if( get_field('form-id') ) {
								gravity_form(get_field('form-id'), get_field('form-show-title'), get_field('form-show-description'), false, [], true);
							}
							?>
						</div>

					</div>
				</div>
			</div>
		</div>

		<?
		include( THEME_PATH . '/includes/careers/bottom.php' );
		?>
	</section>

<? endwhile; endif; get_footer(); ?>