</main>

<footer id="footer">
	<div class="wrapper large">
		<div class="row">
			<div class="col-12 col-md-3">
				<div id="footer-col-1" class="footer-col">
					<?= FW::get_image( get_field('footer-logo-' . LANG, 'option'), [], 'footer-logo' ); ?>
				</div>
			</div>

			<div class="col-12 col-md-3">
				<div id="footer-col-2" class="footer-col">
					<h3 class="footer-col-title"><?= get_field('footer-col-2-title-' . LANG, 'option'); ?></h3>

					<div class="the-content">
						<p>
							<a href="tel:<?= FW::sanitize_phone_number( get_field('phone', 'option') ); ?>"><?= get_field('phone', 'option'); ?></a><br>
							<a href="mailto:<?= antispambot( get_option('admin_email') ); ?>"><?= antispambot( get_option('admin_email') ); ?></a>
						</p>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-3">
				<div id="footer-col-3" class="footer-col">
					<h3 class="footer-col-title"><?= get_field('footer-col-3-title-' . LANG, 'option'); ?></h3>

					<div class="the-content"><?= get_field('footer-col-3-content-' . LANG, 'option'); ?></div>
				</div>
			</div>

			<div class="col-12 col-md-3">
				<div id="footer-col-4" class="footer-col">
					<h3 class="footer-col-title"><?= get_field('footer-col-4-title-' . LANG, 'option'); ?></h3>

					<? include( THEME_PATH . '/includes/social-links.php'); ?>
				</div>
			</div>
		</div>
<style>
	#rubik-api-copyright{
		padding:5px 0;
		justify-content:center;

	}
	
</style>
		<p class="copyright"><?= FW::get_copyright() ?></p>
		<?= FW::rubik_footer(LANG, "black") ?>
	</div>
</footer>

<? wp_footer(); ?>
</body>
</html>