<?
/*
 * Template Name: Home
 */
get_header(); if( have_posts() ) : while( have_posts() ) : the_post(); ?>
	<?
	if( get_field('banner-video-file') ) {
		$video_url = get_field('banner-video-file');
	}
	?>
	<section id="home-banner" style="background-image: url(<?= get_field('banner-video-img'); ?>)">
		<div class="overlay" <? if( get_field('overlay-opacity') ){ ?>style="opacity: <?= get_field('overlay-opacity') / 100; ?>"<? } ?>></div>

		<div class="wrapper">
			<div class="content inner">
				<h1 class="title medium dark-grey wow fadeInUp"><?= get_field('banner-title'); ?></h1>
				<div class="the-content medium-grey wow fadeInUp" data-wow-delay="200ms"><?= get_field('banner-content'); ?></div>

				<? if( get_field('banner-btn') ){ ?>
					<div class="buttons wow fadeInUp" data-wow-delay="350ms">
						<?= FW::button( get_field('banner-btn'), 'btn white' );	?>
					</div>
				<? } ?>
			</div>
		</div>

		<video preload="auto" autoplay muted>
			<source src="<?= $video_url; ?>" type="video/mp4">
		</video>
	</section>

	<section id="home-products">
		<div class="wrapper medium">
			<div class="d-flex align-items-center">
				<div id="home-products-content">
					<? if( get_field('products-subtitle') ){ ?>
						<p class="title tiny wow fadeInUp"><?= get_field('products-subtitle'); ?></p>
					<? } ?>

					<? if( get_field('products-title') ){ ?>
						<h2 class="title medium wow fadeInUp" data-wow-delay="100ms"><?= get_field('products-title'); ?></h2>
					<? } ?>

					<? if( have_rows('products-buttons') ){ ?>
						<div class="buttons wow fadeInUp" data-wow-delay="200ms">
							<? while( have_rows('products-buttons') ){ the_row(); ?>
								<?= FW::button( get_sub_field('btn'), 'btn blue' ); ?>
							<? } ?>
						</div>
					<? } ?>
				</div>
				
				<div id="home-products-diamonds">
					<? include(THEME_PATH . '/includes/products/diamond.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="home-partners">
		<div class="wrapper small">
			<? if( get_field('partners-title') ){ ?>
				<h3 class="title medium blue wow fadeInUp"><?= get_field('partners-title'); ?></h3>
			<? } ?>

			<? if( get_field('partners-content') ){ ?>
				<div class="the-content wow fadeInUp" data-wow-delay="120ms"><?= get_field('partners-content'); ?></div>
			<? } ?>
		</div>

		<? if( have_rows('partners-partners') ){ ?>
			<div class="partners-slider">

				<?
				while( have_rows('partners-partners') ){
					the_row();

					$link = get_sub_field('link'); 	?>

					<div class="partner-block-parent">
						<a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>" class="partner-block" title="<?= $link['title']; ?>" style="background-image: url(<?= get_sub_field('img'); ?>);"></a>

						<? if( get_sub_field('content') ){ ?>
							<div class="the-content"><?= get_sub_field('content'); ?></div>
						<? } ?>
					</div>

				<? } ?>

			</div>
		<? } ?>
	</section>

	<?
	if( get_field('custom-img') ){
		$teaser_btn = get_field('custom-btn');
		?>
		<section id="home-custom">
			<div class="wrapper large">
				<div class="teaser-image-block wow fadeInUp">
					<div class="row no-gutters">
						<div class="col-12 col-lg-6">
							<a href="<?= $teaser_btn['url']; ?>" data-mh="teaser-img" class="teaser-image-img" style="background-image: url(<?= get_field('custom-img'); ?>);"></a>
						</div>

						<div class="col-12 col-lg-6">
							<div data-mh="teaser-img" class="teaser-image-content">
								<? if( get_field('custom-title') ){ ?>
									<h3 class="title small"><?= get_field('custom-title'); ?></h3>
								<? } ?>

								<? if( get_field('custom-content') ){ ?>
									<div class="the-content"><?= get_field('custom-content'); ?></div>
								<? } ?>

								<?
								if( get_field('custom-btn') ){
									echo FW::button( $teaser_btn, 'btn white' );
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<? } ?>

	<?
	$blog = new WP_Query(
		array(
			'post_type' => 'post',
			'posts_per_page' => 3
		)
	);

	if( $blog->have_posts() ){ ?>

		<section id="home-blog">
			<div class="wrapper">
				<h3 class="title small wow fadeInUp"><?= pll__('latest-news'); ?></h3>

				<div class="blog-articles">
					<div class="row">
						<?
						while( $blog->have_posts() ){
							$blog->the_post();

							include(THEME_PATH . '/includes/blog/archive-block.php');
						}
						?>
					</div>
				</div>
			</div>
		</section>

	<? } wp_reset_postdata(); ?>

<? endwhile; endif; get_footer(); ?>