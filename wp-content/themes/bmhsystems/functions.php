<?
require_once 'fw/setup.php';

/*
 * Google Maps
 */
define('GOOGLE_MAPS_API_KEY', get_field('google-maps-api-key', 'option') );

function my_acf_google_map_api( $api ){

	$api['key'] = GOOGLE_MAPS_API_KEY;

	return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/*
 * Change Gravity Forms loader
 */
add_filter('gform_ajax_spinner_url', 'spinner_url', 10, 2);
function spinner_url( $image_src, $form )
{
	return THEME_URL . '/images/loader.svg';
}

/*
 * Edit Yoast breadcrumbs for post types
 */
add_filter( 'wpseo_breadcrumb_links', 'custom_yoast_breadcrumbs' );
function custom_yoast_breadcrumbs( $links )
{
	global $post;

	// Products
	if( get_post_type() == 'product' ){
		$breadcrumb[] = array(
			'url' => get_permalink( FW::get_page_id_by_template('templates/products') ),
			'text' => get_the_title( FW::get_page_id_by_template('templates/products') ),
		);

		array_splice( $links, 1, 0, $breadcrumb );
	}

	return $links;
}

/*
 * Crawl over menu items and add buttons, GTM, etc
 */
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args )
{
	foreach( $items as &$item )
	{
		$is_btn = get_field('show-megamenu', $item);

		if( $is_btn ){
			$item->classes[] = 'show-megamenu';
		}
	}

	return $items;
}