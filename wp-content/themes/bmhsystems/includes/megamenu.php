<?
$main_category = get_field('mm-main-product-category-' . LANG, 'option');
?>

<nav id="megamenu">
	<div class="wrapper">
		<div class="row">
			<?
			if( ! empty($main_category) ){
				$main_category_id = $main_category->term_id;
				?>

				<div class="col-12 col-sm-8">
					<p class="mm-title with-border">
						<a href="<?= get_term_link($main_category); ?>"><?= $main_category->name; ?></a>
					</p>

					<?
					$product_types = get_terms(
						array(
							'taxonomy' => 'product-type',
							'parent' => 0
						)
					);

					if( ! empty($product_types) ){ ?>

						<div class="row">

							<? foreach($product_types as $product_type){ ?>

								<div class="col-12 col-sm-3">
									<ul class="nav">
										<li>
											<a href="<?= get_term_link($product_type); ?>"><?= $product_type->name; ?></a>

											<?
											/*$product_types_children = get_terms(
												array(
													'taxonomy' => 'product-type',
													'parent' => $product_type->term_id
												)
											);

											if( ! empty($product_types_children) ){ ?>

												<ul>

													<? foreach($product_types_children as $product_types_child){ ?>
														<li>
															<a href="<?= get_term_link($product_types_child); ?>"><?= $product_types_child->name; ?></a>
														</li>
													<? } ?>
												</ul>

											<? } */ ?>

											<?
											$product_types_children = get_posts(
												array(
													'post_type' => 'product',
													'posts_per_page' => -1,
													'orderby' => 'menu_order',
													'order' => 'ASC',
													'tax_query' => array(
														array(
															'taxonomy' => 'product-type',
															'terms' => $product_type->term_id
														)
													)
												)
											);

											if( ! empty($product_types_children) ){ ?>

												<ul>

													<? foreach($product_types_children as $product_types_child){ ?>
														<li>
															<a href="<?= get_permalink($product_types_child->ID); ?>"><?= $product_types_child->post_title; ?></a>
														</li>
													<? } ?>
												</ul>

											<? } ?>
										</li>
									</ul>
								</div>

							<? } ?>
						</div>

					<? } ?>
				</div>

			<? } ?>

			<?
			$product_categories = get_terms(
				array(
					'taxonomy' => 'product-category',
					'exclude' => $main_category_id
				)
			);

			if( ! empty($product_categories) ){ ?>

				<div class="col-12 col-sm-4">

					<? foreach($product_categories as $product_category){ ?>
						<p class="mm-title">
							<a href="<?= get_term_link($product_category); ?>"><?= $product_category->name; ?></a>
						</p>
					<? } ?>

					<?
					if( get_field('mm-btn-' . LANG, 'option') ){
						echo FW::button( get_field('mm-btn-' . LANG, 'option'), 'btn white' );
					}
					?>

				</div>

			<? } ?>
		</div>
	</div>
</nav>