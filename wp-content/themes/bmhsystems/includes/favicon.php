<?
$version = 1.1;
?>

<link rel="apple-touch-icon" sizes="180x180" href="<?= THEME_URL; ?>/images/favicon/apple-touch-icon.png?v=<?= $version ?>">
<link rel="icon" type="image/png" href="<?= THEME_URL; ?>/images/favicon/favicon-32x32.png?v=<?= $version ?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?= THEME_URL; ?>/images/favicon/favicon-16x16.png?v=<?= $version ?>" sizes="16x16">
<link rel="manifest" href="<?= THEME_URL; ?>/images/favicon/manifest.json?v=<?= $version ?>">
<link rel="mask-icon" href="<?= THEME_URL; ?>/images/favicon/safari-pinned-tab.svg?v=<?= $version ?>" color="#5bbad5">
<meta name="theme-color" content="#ffffff">