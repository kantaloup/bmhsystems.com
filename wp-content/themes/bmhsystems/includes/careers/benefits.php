<?
$career_page_id = FW::get_page_id_by_template('templates/careers');
?>

<? if( have_rows('benefits', $career_page_id ) ){ ?>

	<div class="career-benefits">

		<?
		$i = 1;
		while( have_rows('benefits', $career_page_id) ){
			the_row();
			$delay = rand(1, 4);
			?>

			<? if($i % 3 == 1){ ?>
				<div class="career-benefits-row">
			<? } ?>

					<div class="career-benefit wow fadeIn bg-<?= get_sub_field('bg-color'); ?> content-<?= get_sub_field('content-color'); ?>" data-wow-delay="<?= $delay * 100; ?>ms">
						<div class="inner">
							<?
							if( get_sub_field('icon') ){
								$icon = get_sub_field('icon');
								$icon_id = $icon['ID'];

								echo file_get_contents( get_attached_file($icon_id) );
							}
							?>

							<? if( get_sub_field('title') ){ ?>
								<h4><?= get_sub_field('title'); ?></h4>
							<? } ?>
						</div>
					</div>


			<? if($i % 3 == 0){ ?>
				</div>
			<? } ?>
		<? $i++; } ?>

	</div>

<? } ?>
