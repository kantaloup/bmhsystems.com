<div class="job-archive-block wow fadeInUp" data-id="<?= get_the_ID(); ?>">
	<a class="job-title" href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a>
	<a class="btn blue" href="<?= get_permalink(); ?>"><?= pll__('job-view-more'); ?></a>
</div>