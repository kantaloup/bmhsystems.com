<?
$career_page_id = FW::get_page_id_by_template('templates/careers');
?>

<section id="career-bottom">
	<div class="row no-gutters">
		<div class="col-12 col-sm-6">
			<div data-mh="career-bottom-col">
				<?
				$jobs = new WP_Query(
					array(
						'post_type' => 'career',
						'posts_per_page' => -1
					)
				);

				if( $jobs->have_posts() ){ ?>

					<div id="available-positions">
						<div class="inner">
							<? if( get_field('positions-title', $career_page_id) ){ ?>
								<h2 class="title small dark-grey wow fadeInUp"><?= get_field('positions-title', $career_page_id); ?></h2>
							<? } ?>

							<?
							while( $jobs->have_posts() ){
								$jobs->the_post();

								include(THEME_PATH . '/includes/careers/archive-block.php');
							}
							?>
						</div>
					</div>

				<? } wp_reset_postdata(); ?>

				<div id="internship-block">
					<? if( get_field('internship-title', $career_page_id) ){ ?>
						<h2 class="title small white wow fadeInUp"><?= get_field('internship-title', $career_page_id); ?></h2>
					<? } ?>

					<? if( get_field('internship-content', $career_page_id) ){ ?>
						<div class="the-content wow fadeInUp"><?= get_field('internship-content', $career_page_id); ?></div>
					<? } ?>

					<? if( have_rows('internship-buttons', $career_page_id) ){ ?>
						<div class="buttons wow fadeInUp">
							<?
							while( have_rows('internship-buttons', $career_page_id) ){ the_row();
								echo FW::button( get_sub_field('btn'), 'btn white' );
							}
							?>
						</div>
					<? } ?>
				</div>
			</div>
		</div>

		<div class="col-12 col-sm-6">
			<div id="forty-reasons" data-mh="career-bottom-col">
				<div class="inner">
					<? if( get_field('reasons-title', $career_page_id) ){ ?>
						<h3 class="title smaller dark-grey small-under wow fadeInUp"><?= get_field('reasons-title', $career_page_id); ?></h3>
					<? } ?>

					<? if( have_rows('reasons', $career_page_id) ){ ?>
						<div class="diamond-list">

							<? while( have_rows('reasons', $career_page_id) ){ the_row(); ?>
								<div class="diamond-list-item wow fadeInUp">
									<div class="number">
										<span><?= get_row_index(); ?></span>
									</div>

									<div class="content">
										<h4><?= get_sub_field('title'); ?></h4>
										<div class="the-content"><?= get_sub_field('content'); ?></div>
									</div>
								</div>
							<? } ?>

						</div>
					<? } ?>

					<? if( have_rows('reasons-buttons', $career_page_id) ){ ?>
						<div class="buttons wow fadeInUp">
							<?
							while( have_rows('reasons-buttons', $career_page_id) ){ the_row();
								echo FW::button( get_sub_field('btn'), 'btn blue' );
							}
							?>
						</div>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</section>
