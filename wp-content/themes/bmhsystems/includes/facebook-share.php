<a class="facebook-share wow fadeInUp" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink(); ?>" onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=0,resizable=0'); return false;">
	<?= file_get_contents( THEME_PATH . '/images/icon-facebook.svg' ); ?>
	<span><?= pll__('share-this-article'); ?></span>
</a>