<ul class="social-icons">
	<? if( get_field('facebook', 'option') ){ ?>
		<li class="facebook-icon">
			<a href="<?= get_field('facebook', 'option'); ?>" target="_blank">
				<?= file_get_contents( THEME_PATH . '/images/icon-facebook.svg' ); ?>
			</a>
		</li>
	<? } ?>

	<? if( get_field('linkedin', 'option') ){ ?>
		<li class="linkedin-icon">
			<a href="<?= get_field('linkedin', 'option'); ?>" target="_blank">
				<?= file_get_contents( THEME_PATH . '/images/icon-linkedin.svg' ); ?>
			</a>
		</li>
	<? } ?>

	<? if( get_field('youtube', 'option') ){ ?>
		<li class="youtube-icon">
			<a href="<?= get_field('youtube', 'option'); ?>" target="_blank">
				<?= file_get_contents( THEME_PATH . '/images/icon-youtube.svg' ); ?>
			</a>
		</li>
	<? } ?>

	<? if( get_field('instagram', 'option') ){ ?>
		<li class="instagram-icon">
			<a href="<?= get_field('instagram', 'option'); ?>" target="_blank">
				<?= file_get_contents( THEME_PATH . '/images/icon-instagram.svg' ); ?>
			</a>
		</li>
	<? } ?>

	<? if( get_field('twitter', 'option') ){ ?>
		<li class="twitter-icon">
			<a href="<?= get_field('twitter', 'option'); ?>" target="_blank">
				<?= file_get_contents( THEME_PATH . '/images/icon-twitter.svg' ); ?>
			</a>
		</li>
	<? } ?>
</ul>