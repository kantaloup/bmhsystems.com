<div class="col-12 col-sm-6 col-xl-4">
	<a href="<?= get_permalink(); ?>" style="background-image: url(<?= wp_get_attachment_url( FW::featured_image() ); ?>);" class="project-archive-block" data-id="<?= get_the_ID(); ?>">
		<div class="content">
			<h4><?= get_the_title(); ?></h4>
		</div>
	</a>
</div>