<div class="col-12 col-sm-6 col-xl-4">
	<article class="blog-archive-block wow fadeInUp" data-id="<?= get_the_ID(); ?>">
		<a class="img" href="<?= get_permalink(); ?>" style="background-image: url(<?= wp_get_attachment_url( FW::featured_image() ); ?>);"></a>

		<div class="content">
			<h4>
				<a href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a>
			</h4>

			<time><?= date_i18n( get_option('date_format'), get_the_time('U') ); ?></time>

			<div class="the-content"><? the_excerpt(); ?></div>

			<div class="category-links">
				<? the_category(', '); ?>
			</div>

			<a href="<?= get_permalink(); ?>" class="btn blue"><?= pll__('read-more'); ?></a>
		</div>
	</article>
</div>