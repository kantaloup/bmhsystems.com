<aside id="sidebar">
	<a href="javascript:;" class="sidebar-trigger">
		<?= file_get_contents( THEME_PATH . '/images/icon-sidebar.svg' ); ?>
	</a>

	<div class="sidebar-widget dark-grey">
		<? if( get_field('sidebar-widget-title', $blog_page_id ) ){ ?>
			<h4 class="widget-title"><?= get_field('sidebar-widget-title', $blog_page_id ); ?></h4>
		<? } ?>

		<? if( get_field('sidebar-widget-content', $blog_page_id ) ){ ?>
			<div class="widget-content the-content">
				<?= get_field('sidebar-widget-content', $blog_page_id ); ?>
			</div>
		<? } ?>

		<?
		if( get_field('sidebar-widget-btn', $blog_page_id ) ){
			echo FW::button( get_field('sidebar-widget-btn', $blog_page_id ), 'btn white full' );
		}
		?>
	</div>

	<div class="sidebar-widget">
		<h4 class="widget-title"><?= pll__('filter-by-category'); ?></h4>

		<nav class="nav-links">
			<ul>
				<? if( $is_category ){ ?>
					<li>
						<a href="<?= get_permalink( $blog_page_id ); ?>"><?= pll__('back-to-blog'); ?></a>
					</li>
				<? } ?>

				<?
				wp_list_categories(
					array(
						'title_li' => ''
					)
				);
				?>
			</ul>
		</nav>
	</div>
</aside>