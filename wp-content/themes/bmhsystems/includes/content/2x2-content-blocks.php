<? if( have_rows('blocks') ){ ?>
	<div class="two-by-two-content-blocks">
		<div class="row no-gutters">

			<? $i = 0; while( have_rows('blocks') ){ the_row(); ?>


				<div class="col-12 col-sm-6">
					<div class="content-block <?= $i == 0 ? 'left' : 'right'; ?> <?= get_sub_field('bg-color'); ?>" data-mh="content-block">

						<div class="inner">
							<? if( get_sub_field('title') ){ ?>
								<h2 class="title small small-under dark-grey"><?= get_sub_field('title'); ?></h2>
							<? } ?>

							<? if( get_sub_field('subtitle') ){ ?>
								<h3 class="title smaller small-under medium-grey"><?= get_sub_field('subtitle'); ?></h3>
							<? } ?>

							<? if( get_sub_field('content') ){ ?>
								<div class="the-content"><?= get_sub_field('content'); ?></div>
							<? } ?>

							<? if( have_rows('buttons') ){ ?>
								<div class="buttons wow fadeInUp" data-wow-delay="350ms">

									<?
									while( have_rows('buttons') ){
										the_row();

										echo FW::button( get_sub_field('btn'), 'btn blue' );
									}
									?>

								</div>
							<? } ?>
						</div>

					</div>
				</div>
			<? $i++; } ?>

		</div>
	</div>
<? } ?>