<div class="two-by-two-content-blocks">
	<div class="row no-gutters">

		<div class="col-12 col-sm-6 <? if( get_sub_field('img-position') == 'right' ){ ?>order-last<? } ?>">
			<div class="content-block img" data-mh="content-block" style="background-image: url(<?= get_sub_field('img'); ?>)"></div>
		</div>

		<div class="col-12 col-sm-6">
			<div class="content-block <? if( get_sub_field('img-position') == 'right' ){ ?>left<? } ?> <?= get_sub_field('bg-color'); ?>" data-mh="content-block">

				<div class="inner">
					<? if( get_sub_field('title') ){ ?>
						<h2 class="title small small-under dark-grey"><?= get_sub_field('title'); ?></h2>
					<? } ?>

					<? if( get_sub_field('subtitle') ){ ?>
						<h3 class="title smaller small-under medium-grey"><?= get_sub_field('subtitle'); ?></h3>
					<? } ?>

					<? if( get_sub_field('content') ){ ?>
						<div class="the-content"><?= get_sub_field('content'); ?></div>
					<? } ?>

					<? if( have_rows('buttons') ){ ?>
						<div class="buttons wow fadeInUp" data-wow-delay="350ms">

							<?
							while( have_rows('buttons') ){
								the_row();

								$type = get_sub_field('btn-type');
								$data = $type == 'video' ? 'data-fancybox' : '';

								echo FW::button( get_sub_field('btn'), 'btn blue', $data );
							}
							?>

						</div>
					<? } ?>
				</div>

			</div>
		</div>

	</div>
</div>