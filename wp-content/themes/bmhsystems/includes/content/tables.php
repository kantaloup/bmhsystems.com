<section class="tables slice">
	<div class="wrapper">
		<? if( get_sub_field('title') ){ ?>
			<h3 class="title small dark-grey"><?= get_sub_field('title'); ?></h3>
		<? } ?>

		<? if( have_rows('tables') ){ ?>

			<div class="spec-tables">
				<div class="row">

					<? while( have_rows('tables') ){ the_row(); ?>

						<div class="col-12 col-sm-4">
							<div class="spec-table">
								<h3><?= get_sub_field('title'); ?></h3>
								<div class="the-content"><?= get_sub_field('content'); ?></div>
							</div>
						</div>

					<? } ?>

				</div>
			</div>

		<? } ?>
	</div>
</section>