<?
$home_id = get_option('page_on_front');
if( have_rows('products-diamonds', $home_id) ){ ?>

	<div class="product-diamonds">
		<?
		while( have_rows('products-diamonds', $home_id) ){
			the_row();
			$url = get_sub_field('btn');
			$delay = rand(1, 3);
			?>

			<a href="<?= $url['url']; ?>" class="product-diamond diamond-<?= get_row_index(); ?> wow fadeIn" data-wow-delay="<?= $delay * 100; ?>ms">
				<div class="inner">
					<?= file_get_contents( get_sub_field('icon') ); ?>
					<span><?= $url['title']; ?></span>
				</div>
			</a>

		<? } ?>
	</div>

<? } ?>