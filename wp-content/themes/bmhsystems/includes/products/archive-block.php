<div class="col-12 col-sm-6 col-xl-4">
	<div class="product-archive-block" data-id="<?= get_the_ID(); ?>">
		<a class="fi" href="<?= get_permalink(); ?>" style="background-image: url(<?= wp_get_attachment_url( FW::featured_image() ); ?>);"></a>
		<div class="meta">
			<h2>
				<a href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a>
			</h2>

			<? if( get_field( 'archive-content') ){ ?>
				<div class="the-content">
					<?= get_field('archive-content'); ?>
				</div>
			<? } ?>
		</div>

	</div>
</div>