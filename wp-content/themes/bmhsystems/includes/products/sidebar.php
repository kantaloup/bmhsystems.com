<aside id="sidebar">
	<a href="javascript:;" class="sidebar-trigger">
		<?= file_get_contents( THEME_PATH . '/images/icon-sidebar.svg' ); ?>
	</a>

	<div class="sidebar-widget dark-grey">
		<? if( get_field('sidebar-widget-title', $products_page_id ) ){ ?>
			<h4 class="widget-title"><?= get_field('sidebar-widget-title', $products_page_id ); ?></h4>
		<? } ?>

		<? if( get_field('sidebar-widget-content', $products_page_id ) ){ ?>
			<div class="widget-content the-content">
				<?= get_field('sidebar-widget-content', $products_page_id ); ?>
			</div>
		<? } ?>

		<?
		if( get_field('sidebar-widget-btn', $products_page_id ) ){
			echo FW::button( get_field('sidebar-widget-btn', $products_page_id ), 'btn white full' );
		}
		?>
	</div>

	<div class="sidebar-widget">
		<h4 class="widget-title"><?= pll__('filter-by-category'); ?></h4>

		<nav class="nav-links">
			<ul>
				<? if( $is_category ){ ?>
					<li>
						<a href="<?= get_permalink( $products_page_id ); ?>"><?= pll__('back-to-products'); ?></a>
					</li>
				<? } ?>

				<?
				wp_list_categories(
					array(
						'title_li' => '',
						'taxonomy' => 'product-category'
					)
				);
				?>
			</ul>
		</nav>
	</div>

	<div class="sidebar-widget">
		<h4 class="widget-title"><?= pll__('filter-by-type'); ?></h4>

		<nav class="nav-links">
			<ul>
				<? if( $is_category ){ ?>
					<li>
						<a href="<?= get_permalink( $products_page_id ); ?>"><?= pll__('back-to-products'); ?></a>
					</li>
				<? } ?>

				<?
				$product_types = get_terms(
					array(
						'taxonomy' => 'product-type',
						'parent' => 0
					)
				);

				if( ! empty($product_types) ){ ?>
					<? foreach($product_types as $product_type){ ?>

						<li>
							<a href="<?= get_term_link($product_type); ?>"><?= $product_type->name; ?></a>

							<?
							/*$product_types_children = get_terms(
								array(
									'taxonomy' => 'product-type',
									'parent' => $product_type->term_id
								)
							);

							if( ! empty($product_types_children) ){ ?>

								<ul>

									<? foreach($product_types_children as $product_types_child){ ?>
										<li>
											<a href="<?= get_term_link($product_types_child); ?>"><?= $product_types_child->name; ?></a>
										</li>
									<? } ?>
								</ul>

							<? } */ ?>

							<?
							$product_types_children = get_posts(
								array(
									'post_type' => 'product',
									'posts_per_page' => -1,
									'orderby' => 'menu_order',
									'order' => 'ASC',
									'tax_query' => array(
										array(
											'taxonomy' => 'product-type',
											'terms' => $product_type->term_id
										)
									)
								)
							);

							if( ! empty($product_types_children) ){ ?>

								<ul>

									<? foreach($product_types_children as $product_types_child){ ?>
										<li>
											<a href="<?= get_permalink($product_types_child->ID); ?>"><?= $product_types_child->post_title; ?></a>
										</li>
									<? } ?>
								</ul>

							<? } ?>
						</li>

					<? } ?>
				<? } ?>
			</ul>
		</nav>
	</div>
</aside>